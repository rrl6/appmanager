import axios from 'axios';
import alt from '../alt';
import AppActions from '../actions/AppActions.jsx';
import AuthActions from '../actions/AuthActions.jsx';
import AuthStore from '../stores/AuthStore.jsx';
import Config from '../components/Config.jsx';

class AppStore {
  constructor() {
    this.bindActions(AppActions);

    this.userApps = Config.getUserApps();
    this.activeUserApp = {};
    this.addingNewApp = false;  // this is used to track cancel/add new app button below the list
    this.error = {};
    this.showRefreshModal = false;
    this.showNoRefreshModal = false;
    this.user = undefined;
    this.editing = false;
    this.activeIdx = -1;
    this.activeUserAppBackup = null;
  }

  onRefreshUser(user) {
    this.setState({
      user,
    });
  }

  onRefreshNewAppName(name) {
    const { activeUserApp, userApps, activeIdx } = this;
    activeUserApp.displayName = userApps[activeIdx].displayName = name;
    this.setState({
      activeUserApp,
      userApps,
    });
  }

  onSyncClientId(name) {
    if (name.length === 0) {
      const { activeUserApp } = this;
      activeUserApp.clientId = '';
      return;
    }
    let convertedClientId = '';
    for (const c of name) {
      if (/^[A-Z]+$/.test(c)) {
        convertedClientId += c.toLowerCase();
      } else if (/^[a-z0-9-]+$/i.test(c)) {
        convertedClientId += c;
      } else {
        convertedClientId += '-';
      }
    }
    const { activeUserApp, userApps, activeIdx } = this;
    activeUserApp.clientId = userApps[activeIdx].clientId = convertedClientId;
    this.setState({
      activeUserApp,
      userApps,
    });
  }

  onSyncActiveUserApp(newActiveUserApp) {
    const { userApps, activeIdx } = this;
    let { activeUserApp } = this;
    activeUserApp = userApps[activeIdx] = Object.assign({}, activeUserApp, newActiveUserApp);
    this.setState({
      activeUserApp,
      userApps,
      activeIdx,
    });
  }

  onAddUserApp() {
    const { userApps } = this;
    let { activeUserApp, addingNewApp, activeIdx } = this;
    activeUserApp = {
      newApp: true,
      redirectURIs: ['http://apidocs.colab.duke.edu/o2c.html', ''],
      privacyURL: Config.getDefaultPrivacyURL(),
    };
    userApps.push(activeUserApp);
    addingNewApp = true;
    activeIdx = userApps.length - 1;
    this.setState({ userApps, activeUserApp, addingNewApp, activeIdx });
  }

  onDeleteUserApp(staleApp) {
    axios
      .delete('https://api.colab.duke.edu/meta/v1/apps', {
        headers: {
          'x-api-key': Config.getClientId(),
          Authorization: `Bearer ${AuthStore.getState().accessToken}`,
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        data: staleApp,
      })
      .then(() => {
        let { userApps, activeUserApp } = this;
        userApps = userApps.filter((item) => {
          return item !== staleApp;
        });
        activeUserApp = {};
        this.setState({
          activeUserApp,
          userApps,
          activeIdx: -1,
        });
      })
      .catch((err) => {
        AppActions.handleError({ type: 'delete_error', body: err });
      });
  }

  onCancelAddUserApp() {
    const { userApps } = this;
    let { activeUserApp, addingNewApp } = this;
    activeUserApp = {};
    userApps.pop();
    addingNewApp = false;
    this.setState({ userApps, activeUserApp, addingNewApp, activeIdx: -1 });
  }

  onSetActiveUserApp(newActiveUserApp) {
    let { activeUserApp } = this;
    const { userApps } = this;
    activeUserApp = newActiveUserApp;
    const idx = userApps.indexOf(activeUserApp);
    if (idx === -1) {
      AppActions.handleError({ type: 'application_error', body: idx });
    }
    this.setState({ activeUserApp, activeIdx: idx });
  }

  onSubmitUserApp(newAppReq) {
    // console.info(newAppReq);
    // console.log(JSON.stringify(newAppReq));
    axios
      .post('https://api.colab.duke.edu/meta/v1/apps', JSON.stringify(newAppReq), {
        headers: {
          'x-api-key': Config.getClientId(),
          Authorization: `Bearer ${AuthStore.getState().accessToken}`,
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      })
      .then((res) => {
        // console.info('response', res.data);
        const { userApps, activeIdx } = this;
        let { activeUserApp, addingNewApp, editing } = this;
        if (!editing) {
          userApps.pop();
          userApps.push(res.data);
        } else {
          userApps[activeIdx] = res.data;
        }
        activeUserApp = res.data;
        addingNewApp = false;
        editing = false;
        this.setState({
          userApps,
          activeUserApp,
          addingNewApp,
          editing,
        });
      })
      .catch((err) => {
        AppActions.handleError({ type: 'submit_error', body: err });
      });
  }

  onGetUserApps() {
    axios
      .get('https://api.colab.duke.edu/meta/v1/apps', {
        headers: {
          'x-api-key': Config.getClientId(),
          Authorization: `Bearer ${AuthStore.getState().accessToken}`,
          Accept: 'application/json',
        },
      })
      .then((res) => {
        const userApps = res.data;
        this.setState({
          user: AuthStore.getState().user,
          userApps,
        });
      })
      .catch((err) => {
        if (err.response && err.response.status === 401 && err.response.data.error === 'Thrown out by the AuthManager: Couldn\'t determine scopes for this token.') {
          AuthActions.reInit();
        } else {
          AppActions.handleError({ type: 'list_error', body: err });
        }
      });
  }

  onHandleError(err) {
    switch (err.type) {
      case 'list_error':
        this.setState({
          error: {
            type: 'refresh',
            msg: 'An error occurred while retrieving your application list. Click the button below to retry.',
          },
        });
        break;
      case 'identity_error':
        this.setState({
          error: {
            type: 'no_refresh',
            msg: 'An error occurred while retrieving your identity. Please refresh the page at some other time to retry.',
          },
        });
        break;
      case 'submit_error':
        this.setState({
          error: {
            type: 'no_refresh',
            msg: 'An error occurred during submitting your request. Please retry at some other time.',
          },
        });
        break;
      case 'delete_error':
        this.setState({
          error: {
            type: 'no_refresh',
            msg: 'An error occurred during deleting your app. Please retry at some other some.',
          },
        });
        break;
      case 'state_mismatch':
        this.setState({
          error: {
            type: 'refresh',
            msg: 'An error while authenticating. Click the button below to retry.',
          },
        });
        break;
      case 'storage_error':
        this.setState({
          error: {
            type: 'no_refresh',
            msg: 'An error occurred in while trying to cache your login state. This is likely due to private/incognito mode of the browser you are using. The error will not prevent you from using the App Manager, but you may need to login again if you refresh the page.',
          },
        });
        break;
      case 'application_error':
        this.setState({
          error: {
            type: 'refresh',
            msg: 'An internal application error occured.',
          },
        });
        break;
      default:
        this.setState({
          error: {
            type: 'no_refresh',
            msg: 'An unknown error has occured',
          },
        });
    }

    const { error } = this;
    if (error.type === 'no_refresh') {
      this.setState({
        showRefreshModal: false,
        showNoRefreshModal: true,
      });
    } else {
      this.setState({
        showRefreshModal: true,
        showNoRefreshModal: false,
      });
    }
  }

  onCloseModal() {
    this.setState({
      showNoRefreshModal: false,
      error: {},
    });
  }

  onEditApp(isEditing) {
    let { activeUserApp, activeUserAppBackup } = this;
    const { userApps } = this;
    if (isEditing) {
      activeUserAppBackup = Object.assign({}, activeUserApp);
      this.setState({
        editing: true,
        activeUserAppBackup,
      });
    } else { // cancel editing, discard all changes and restore old app
      userApps.forEach((item, idx) => {
        if (item === activeUserApp) {
          userApps[idx] = activeUserAppBackup;
        }
      });
      activeUserApp = Object.assign({}, activeUserAppBackup);
      this.setState({
        editing: false,
        activeUserApp,
        userApps,
      });
    }
  }
}

export default alt.createStore(AppStore, 'AppStore');
